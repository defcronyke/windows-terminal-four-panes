Windows Terminal Four Panes  
===========================  
  
For some reason it's really annoying to figure out how to
set up Windows Terminal to open with four panes on a single 
tab when you launch it. Three panes is no problem, but if 
you want four, you currently need to do something similar 
to this example here.  
  
To do it, modify your Windows Terminal settings.json file
so that it's similar to the one in this repository.  
  
Note that this example is assuming the newer cross-platform
PowerShell version from GitHub is installed, and it's
set to open two Debian WSL terminals, as well as two 
PowerShell terminals, but you can easily change it to open 
whichever kinds of terminals you'd prefer instead. It will
also maximize the window after opening it, because that's
what I prefer personally.  
  
  
If you're interested in just how stupid this workaround is,
it works as follows:  
  
1. Open one terminal in the entire window.  

2. Split it in half vertically and open a different terminal in the right half.  

3. Split that new one in half horizontally, and instead of telling it to open another terminal at the bottom, tell it to run PowerShell with some wild parameters which do as follows:  
  
    a. Maximize the window (because I prefer it maximized).  
  
    b. Send an automatic keypress causing focus to shift to the left-most terminal.  
  
    c. Send another keypress causing that terminal to split and duplicate itself, becoming the desired fourth terminal on the lower-left side.  
  
    d. Send one more keypress causing focus to shift to the top-left of the four terminals.  
  
    e. Some extra parameters which cause this instance of PowerShell which is doing all these things to be running in the user's home directory and then to not exit itself once it's finished, so then it will leave open that third terminal so we still have four once it's done.  
  
It's very stupid, but this solves the issue, and I don't think there's a better way.  
